// package: fcp.accounting.v1.auth_private
// file: v1/auth_private/auth_service.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_auth_common_common_pb from "../../v1/auth_common/common_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";

export class UserBriefForMessage extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserBriefForMessage.AsObject;
  static toObject(includeInstance: boolean, msg: UserBriefForMessage): UserBriefForMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserBriefForMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserBriefForMessage;
  static deserializeBinaryFromReader(message: UserBriefForMessage, reader: jspb.BinaryReader): UserBriefForMessage;
}

export namespace UserBriefForMessage {
  export type AsObject = {
    id: string,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    email: string,
  }
}

export class GetUsersByIdsRequest extends jspb.Message {
  clearIdsList(): void;
  getIdsList(): Array<string>;
  setIdsList(value: Array<string>): void;
  addIds(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUsersByIdsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUsersByIdsRequest): GetUsersByIdsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUsersByIdsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUsersByIdsRequest;
  static deserializeBinaryFromReader(message: GetUsersByIdsRequest, reader: jspb.BinaryReader): GetUsersByIdsRequest;
}

export namespace GetUsersByIdsRequest {
  export type AsObject = {
    idsList: Array<string>,
  }
}

export class GetUsersByIdsResponse extends jspb.Message {
  clearUsersList(): void;
  getUsersList(): Array<UserBriefForMessage>;
  setUsersList(value: Array<UserBriefForMessage>): void;
  addUsers(value?: UserBriefForMessage, index?: number): UserBriefForMessage;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUsersByIdsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUsersByIdsResponse): GetUsersByIdsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUsersByIdsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUsersByIdsResponse;
  static deserializeBinaryFromReader(message: GetUsersByIdsResponse, reader: jspb.BinaryReader): GetUsersByIdsResponse;
}

export namespace GetUsersByIdsResponse {
  export type AsObject = {
    usersList: Array<UserBriefForMessage.AsObject>,
  }
}

export class GetUsersByPatternForMessageRequest extends jspb.Message {
  getPattern(): string;
  setPattern(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUsersByPatternForMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUsersByPatternForMessageRequest): GetUsersByPatternForMessageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUsersByPatternForMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUsersByPatternForMessageRequest;
  static deserializeBinaryFromReader(message: GetUsersByPatternForMessageRequest, reader: jspb.BinaryReader): GetUsersByPatternForMessageRequest;
}

export namespace GetUsersByPatternForMessageRequest {
  export type AsObject = {
    pattern: string,
  }
}

export class GetUsersByPatternForMessageResponse extends jspb.Message {
  clearUsersList(): void;
  getUsersList(): Array<UserBriefForMessage>;
  setUsersList(value: Array<UserBriefForMessage>): void;
  addUsers(value?: UserBriefForMessage, index?: number): UserBriefForMessage;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUsersByPatternForMessageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUsersByPatternForMessageResponse): GetUsersByPatternForMessageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUsersByPatternForMessageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUsersByPatternForMessageResponse;
  static deserializeBinaryFromReader(message: GetUsersByPatternForMessageResponse, reader: jspb.BinaryReader): GetUsersByPatternForMessageResponse;
}

export namespace GetUsersByPatternForMessageResponse {
  export type AsObject = {
    usersList: Array<UserBriefForMessage.AsObject>,
  }
}

export class SendCodeRequest extends jspb.Message {
  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendCodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendCodeRequest): SendCodeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendCodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendCodeRequest;
  static deserializeBinaryFromReader(message: SendCodeRequest, reader: jspb.BinaryReader): SendCodeRequest;
}

export namespace SendCodeRequest {
  export type AsObject = {
    phoneNumber: string,
  }
}

export class CheckCodeRequest extends jspb.Message {
  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckCodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CheckCodeRequest): CheckCodeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckCodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckCodeRequest;
  static deserializeBinaryFromReader(message: CheckCodeRequest, reader: jspb.BinaryReader): CheckCodeRequest;
}

export namespace CheckCodeRequest {
  export type AsObject = {
    phoneNumber: string,
    code: string,
  }
}

export class CheckCodeResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckCodeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CheckCodeResponse): CheckCodeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckCodeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckCodeResponse;
  static deserializeBinaryFromReader(message: CheckCodeResponse, reader: jspb.BinaryReader): CheckCodeResponse;
}

export namespace CheckCodeResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class CheckUserByLoginRequest extends jspb.Message {
  getLogin(): string;
  setLogin(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckUserByLoginRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CheckUserByLoginRequest): CheckUserByLoginRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckUserByLoginRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckUserByLoginRequest;
  static deserializeBinaryFromReader(message: CheckUserByLoginRequest, reader: jspb.BinaryReader): CheckUserByLoginRequest;
}

export namespace CheckUserByLoginRequest {
  export type AsObject = {
    login: string,
  }
}

export class CheckUserByLoginResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckUserByLoginResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CheckUserByLoginResponse): CheckUserByLoginResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckUserByLoginResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckUserByLoginResponse;
  static deserializeBinaryFromReader(message: CheckUserByLoginResponse, reader: jspb.BinaryReader): CheckUserByLoginResponse;
}

export namespace CheckUserByLoginResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class GetUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserRequest): GetUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserRequest;
  static deserializeBinaryFromReader(message: GetUserRequest, reader: jspb.BinaryReader): GetUserRequest;
}

export namespace GetUserRequest {
  export type AsObject = {
    userId: string,
  }
}

export class ListUsersRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): UserFilter | undefined;
  setFilter(value?: UserFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUsersRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListUsersRequest): ListUsersRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUsersRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUsersRequest;
  static deserializeBinaryFromReader(message: ListUsersRequest, reader: jspb.BinaryReader): ListUsersRequest;
}

export namespace ListUsersRequest {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationRequest.AsObject,
    filter?: UserFilter.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class UserFilter extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): google_protobuf_wrappers_pb.StringValue | undefined;
  setRole(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_auth_common_common_pb.TimeRange | undefined;
  setCreated(value?: v1_auth_common_common_pb.TimeRange): void;

  hasLocked(): boolean;
  clearLocked(): void;
  getLocked(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setLocked(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasLogin(): boolean;
  clearLogin(): void;
  getLogin(): google_protobuf_wrappers_pb.StringValue | undefined;
  setLogin(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserFilter.AsObject;
  static toObject(includeInstance: boolean, msg: UserFilter): UserFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserFilter;
  static deserializeBinaryFromReader(message: UserFilter, reader: jspb.BinaryReader): UserFilter;
}

export namespace UserFilter {
  export type AsObject = {
    role?: google_protobuf_wrappers_pb.StringValue.AsObject,
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_auth_common_common_pb.TimeRange.AsObject,
    locked?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    login?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListUsersResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationResponse): void;

  clearUsersList(): void;
  getUsersList(): Array<v1_auth_common_dtos_pb.User>;
  setUsersList(value: Array<v1_auth_common_dtos_pb.User>): void;
  addUsers(value?: v1_auth_common_dtos_pb.User, index?: number): v1_auth_common_dtos_pb.User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUsersResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListUsersResponse): ListUsersResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUsersResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUsersResponse;
  static deserializeBinaryFromReader(message: ListUsersResponse, reader: jspb.BinaryReader): ListUsersResponse;
}

export namespace ListUsersResponse {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationResponse.AsObject,
    usersList: Array<v1_auth_common_dtos_pb.User.AsObject>,
  }
}

export class CreateUserRequest extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  getRawPassword(): string;
  setRawPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateUserRequest): CreateUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateUserRequest;
  static deserializeBinaryFromReader(message: CreateUserRequest, reader: jspb.BinaryReader): CreateUserRequest;
}

export namespace CreateUserRequest {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
    rawPassword: string,
  }
}

export class UpdateUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateUserRequest): UpdateUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateUserRequest;
  static deserializeBinaryFromReader(message: UpdateUserRequest, reader: jspb.BinaryReader): UpdateUserRequest;
}

export namespace UpdateUserRequest {
  export type AsObject = {
    userId: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    state: number,
    note: string,
  }
}

export class CheckPasswordRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckPasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CheckPasswordRequest): CheckPasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckPasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckPasswordRequest;
  static deserializeBinaryFromReader(message: CheckPasswordRequest, reader: jspb.BinaryReader): CheckPasswordRequest;
}

export namespace CheckPasswordRequest {
  export type AsObject = {
    userId: string,
    password: string,
  }
}

export class CheckPasswordResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckPasswordResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CheckPasswordResponse): CheckPasswordResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckPasswordResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckPasswordResponse;
  static deserializeBinaryFromReader(message: CheckPasswordResponse, reader: jspb.BinaryReader): CheckPasswordResponse;
}

export namespace CheckPasswordResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class ChangePasswordRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getOldPassword(): string;
  setOldPassword(value: string): void;

  getNewPassword(): string;
  setNewPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChangePasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ChangePasswordRequest): ChangePasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ChangePasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChangePasswordRequest;
  static deserializeBinaryFromReader(message: ChangePasswordRequest, reader: jspb.BinaryReader): ChangePasswordRequest;
}

export namespace ChangePasswordRequest {
  export type AsObject = {
    userId: string,
    oldPassword: string,
    newPassword: string,
  }
}

export class LockUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getLockReason(): string;
  setLockReason(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LockUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LockUserRequest): LockUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LockUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LockUserRequest;
  static deserializeBinaryFromReader(message: LockUserRequest, reader: jspb.BinaryReader): LockUserRequest;
}

export namespace LockUserRequest {
  export type AsObject = {
    userId: string,
    lockReason: string,
  }
}

export class UnlockUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnlockUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UnlockUserRequest): UnlockUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnlockUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnlockUserRequest;
  static deserializeBinaryFromReader(message: UnlockUserRequest, reader: jspb.BinaryReader): UnlockUserRequest;
}

export namespace UnlockUserRequest {
  export type AsObject = {
    userId: string,
  }
}

export class InitPasswordResetRequest extends jspb.Message {
  getLogin(): string;
  setLogin(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InitPasswordResetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InitPasswordResetRequest): InitPasswordResetRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InitPasswordResetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InitPasswordResetRequest;
  static deserializeBinaryFromReader(message: InitPasswordResetRequest, reader: jspb.BinaryReader): InitPasswordResetRequest;
}

export namespace InitPasswordResetRequest {
  export type AsObject = {
    login: string,
  }
}

export class ValidatePasswordResetTokenRequest extends jspb.Message {
  getResetToken(): string;
  setResetToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidatePasswordResetTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidatePasswordResetTokenRequest): ValidatePasswordResetTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidatePasswordResetTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidatePasswordResetTokenRequest;
  static deserializeBinaryFromReader(message: ValidatePasswordResetTokenRequest, reader: jspb.BinaryReader): ValidatePasswordResetTokenRequest;
}

export namespace ValidatePasswordResetTokenRequest {
  export type AsObject = {
    resetToken: string,
  }
}

export class ValidatePasswordResetTokenResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidatePasswordResetTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidatePasswordResetTokenResponse): ValidatePasswordResetTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidatePasswordResetTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidatePasswordResetTokenResponse;
  static deserializeBinaryFromReader(message: ValidatePasswordResetTokenResponse, reader: jspb.BinaryReader): ValidatePasswordResetTokenResponse;
}

export namespace ValidatePasswordResetTokenResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class ResetPasswordRequest extends jspb.Message {
  getResetToken(): string;
  setResetToken(value: string): void;

  getNewPassword(): string;
  setNewPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResetPasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ResetPasswordRequest): ResetPasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResetPasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResetPasswordRequest;
  static deserializeBinaryFromReader(message: ResetPasswordRequest, reader: jspb.BinaryReader): ResetPasswordRequest;
}

export namespace ResetPasswordRequest {
  export type AsObject = {
    resetToken: string,
    newPassword: string,
  }
}

export class GetUserResetTokenRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserResetTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserResetTokenRequest): GetUserResetTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserResetTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserResetTokenRequest;
  static deserializeBinaryFromReader(message: GetUserResetTokenRequest, reader: jspb.BinaryReader): GetUserResetTokenRequest;
}

export namespace GetUserResetTokenRequest {
  export type AsObject = {
    userId: string,
  }
}

export class GetUserResetTokenResponse extends jspb.Message {
  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserResetTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserResetTokenResponse): GetUserResetTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserResetTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserResetTokenResponse;
  static deserializeBinaryFromReader(message: GetUserResetTokenResponse, reader: jspb.BinaryReader): GetUserResetTokenResponse;
}

export namespace GetUserResetTokenResponse {
  export type AsObject = {
    code: string,
  }
}

export class GetRoleRequest extends jspb.Message {
  getRole(): string;
  setRole(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRoleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRoleRequest): GetRoleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRoleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRoleRequest;
  static deserializeBinaryFromReader(message: GetRoleRequest, reader: jspb.BinaryReader): GetRoleRequest;
}

export namespace GetRoleRequest {
  export type AsObject = {
    role: string,
  }
}

export class GetRoleResponse extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.RoleFull | undefined;
  setRole(value?: v1_auth_common_dtos_pb.RoleFull): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRoleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetRoleResponse): GetRoleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRoleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRoleResponse;
  static deserializeBinaryFromReader(message: GetRoleResponse, reader: jspb.BinaryReader): GetRoleResponse;
}

export namespace GetRoleResponse {
  export type AsObject = {
    role?: v1_auth_common_dtos_pb.RoleFull.AsObject,
  }
}

export class CreateRoleRequest extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.Role | undefined;
  setRole(value?: v1_auth_common_dtos_pb.Role): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateRoleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateRoleRequest): CreateRoleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateRoleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateRoleRequest;
  static deserializeBinaryFromReader(message: CreateRoleRequest, reader: jspb.BinaryReader): CreateRoleRequest;
}

export namespace CreateRoleRequest {
  export type AsObject = {
    role?: v1_auth_common_dtos_pb.Role.AsObject,
  }
}

export class CreateRoleResponse extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.RoleFull | undefined;
  setRole(value?: v1_auth_common_dtos_pb.RoleFull): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateRoleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateRoleResponse): CreateRoleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateRoleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateRoleResponse;
  static deserializeBinaryFromReader(message: CreateRoleResponse, reader: jspb.BinaryReader): CreateRoleResponse;
}

export namespace CreateRoleResponse {
  export type AsObject = {
    role?: v1_auth_common_dtos_pb.RoleFull.AsObject,
  }
}

export class UpdateRoleRequest extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.Role | undefined;
  setRole(value?: v1_auth_common_dtos_pb.Role): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateRoleRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateRoleRequest): UpdateRoleRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateRoleRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateRoleRequest;
  static deserializeBinaryFromReader(message: UpdateRoleRequest, reader: jspb.BinaryReader): UpdateRoleRequest;
}

export namespace UpdateRoleRequest {
  export type AsObject = {
    role?: v1_auth_common_dtos_pb.Role.AsObject,
  }
}

export class UpdateRoleResponse extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.RoleFull | undefined;
  setRole(value?: v1_auth_common_dtos_pb.RoleFull): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateRoleResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateRoleResponse): UpdateRoleResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateRoleResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateRoleResponse;
  static deserializeBinaryFromReader(message: UpdateRoleResponse, reader: jspb.BinaryReader): UpdateRoleResponse;
}

export namespace UpdateRoleResponse {
  export type AsObject = {
    role?: v1_auth_common_dtos_pb.RoleFull.AsObject,
  }
}

export class ListRolesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): RoleFilter | undefined;
  setFilter(value?: RoleFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRolesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListRolesRequest): ListRolesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListRolesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRolesRequest;
  static deserializeBinaryFromReader(message: ListRolesRequest, reader: jspb.BinaryReader): ListRolesRequest;
}

export namespace ListRolesRequest {
  export type AsObject = {
    filter?: RoleFilter.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class RoleFilter extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): google_protobuf_wrappers_pb.StringValue | undefined;
  setRole(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPermission(): boolean;
  clearPermission(): void;
  getPermission(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPermission(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleFilter.AsObject;
  static toObject(includeInstance: boolean, msg: RoleFilter): RoleFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoleFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleFilter;
  static deserializeBinaryFromReader(message: RoleFilter, reader: jspb.BinaryReader): RoleFilter;
}

export namespace RoleFilter {
  export type AsObject = {
    role?: google_protobuf_wrappers_pb.StringValue.AsObject,
    permission?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListRolesResponse extends jspb.Message {
  clearRolesList(): void;
  getRolesList(): Array<v1_auth_common_dtos_pb.RoleFull>;
  setRolesList(value: Array<v1_auth_common_dtos_pb.RoleFull>): void;
  addRoles(value?: v1_auth_common_dtos_pb.RoleFull, index?: number): v1_auth_common_dtos_pb.RoleFull;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRolesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListRolesResponse): ListRolesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListRolesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRolesResponse;
  static deserializeBinaryFromReader(message: ListRolesResponse, reader: jspb.BinaryReader): ListRolesResponse;
}

export namespace ListRolesResponse {
  export type AsObject = {
    rolesList: Array<v1_auth_common_dtos_pb.RoleFull.AsObject>,
  }
}

export class GetPermissionRequest extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPermissionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPermissionRequest): GetPermissionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPermissionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPermissionRequest;
  static deserializeBinaryFromReader(message: GetPermissionRequest, reader: jspb.BinaryReader): GetPermissionRequest;
}

export namespace GetPermissionRequest {
  export type AsObject = {
    permission: string,
  }
}

export class GetPermissionResponse extends jspb.Message {
  hasPermission(): boolean;
  clearPermission(): void;
  getPermission(): v1_auth_common_dtos_pb.PermissionFull | undefined;
  setPermission(value?: v1_auth_common_dtos_pb.PermissionFull): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPermissionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPermissionResponse): GetPermissionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPermissionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPermissionResponse;
  static deserializeBinaryFromReader(message: GetPermissionResponse, reader: jspb.BinaryReader): GetPermissionResponse;
}

export namespace GetPermissionResponse {
  export type AsObject = {
    permission?: v1_auth_common_dtos_pb.PermissionFull.AsObject,
  }
}

export class ListPermissionsRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PermissionFilter | undefined;
  setFilter(value?: PermissionFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPermissionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPermissionsRequest): ListPermissionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPermissionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPermissionsRequest;
  static deserializeBinaryFromReader(message: ListPermissionsRequest, reader: jspb.BinaryReader): ListPermissionsRequest;
}

export namespace ListPermissionsRequest {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationRequest.AsObject,
    filter?: PermissionFilter.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class PermissionFilter extends jspb.Message {
  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PermissionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PermissionFilter): PermissionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PermissionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PermissionFilter;
  static deserializeBinaryFromReader(message: PermissionFilter, reader: jspb.BinaryReader): PermissionFilter;
}

export namespace PermissionFilter {
  export type AsObject = {
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListPermissionsResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationResponse): void;

  clearPermissionsList(): void;
  getPermissionsList(): Array<v1_auth_common_dtos_pb.PermissionFull>;
  setPermissionsList(value: Array<v1_auth_common_dtos_pb.PermissionFull>): void;
  addPermissions(value?: v1_auth_common_dtos_pb.PermissionFull, index?: number): v1_auth_common_dtos_pb.PermissionFull;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPermissionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPermissionsResponse): ListPermissionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPermissionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPermissionsResponse;
  static deserializeBinaryFromReader(message: ListPermissionsResponse, reader: jspb.BinaryReader): ListPermissionsResponse;
}

export namespace ListPermissionsResponse {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationResponse.AsObject,
    permissionsList: Array<v1_auth_common_dtos_pb.PermissionFull.AsObject>,
  }
}

export class UpdatePermissionRequest extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePermissionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePermissionRequest): UpdatePermissionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePermissionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePermissionRequest;
  static deserializeBinaryFromReader(message: UpdatePermissionRequest, reader: jspb.BinaryReader): UpdatePermissionRequest;
}

export namespace UpdatePermissionRequest {
  export type AsObject = {
    permission: string,
    description: string,
  }
}

export class UpdatePermissionResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePermissionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePermissionResponse): UpdatePermissionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePermissionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePermissionResponse;
  static deserializeBinaryFromReader(message: UpdatePermissionResponse, reader: jspb.BinaryReader): UpdatePermissionResponse;
}

export namespace UpdatePermissionResponse {
  export type AsObject = {
  }
}

export class CreatePermissionRequest extends jspb.Message {
  hasPermission(): boolean;
  clearPermission(): void;
  getPermission(): v1_auth_common_dtos_pb.Permission | undefined;
  setPermission(value?: v1_auth_common_dtos_pb.Permission): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreatePermissionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreatePermissionRequest): CreatePermissionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreatePermissionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreatePermissionRequest;
  static deserializeBinaryFromReader(message: CreatePermissionRequest, reader: jspb.BinaryReader): CreatePermissionRequest;
}

export namespace CreatePermissionRequest {
  export type AsObject = {
    permission?: v1_auth_common_dtos_pb.Permission.AsObject,
  }
}

export class CreatePermissionResponse extends jspb.Message {
  hasPermission(): boolean;
  clearPermission(): void;
  getPermission(): v1_auth_common_dtos_pb.PermissionFull | undefined;
  setPermission(value?: v1_auth_common_dtos_pb.PermissionFull): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreatePermissionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreatePermissionResponse): CreatePermissionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreatePermissionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreatePermissionResponse;
  static deserializeBinaryFromReader(message: CreatePermissionResponse, reader: jspb.BinaryReader): CreatePermissionResponse;
}

export namespace CreatePermissionResponse {
  export type AsObject = {
    permission?: v1_auth_common_dtos_pb.PermissionFull.AsObject,
  }
}

export class DeletePermissionRequest extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePermissionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePermissionRequest): DeletePermissionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePermissionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePermissionRequest;
  static deserializeBinaryFromReader(message: DeletePermissionRequest, reader: jspb.BinaryReader): DeletePermissionRequest;
}

export namespace DeletePermissionRequest {
  export type AsObject = {
    permission: string,
  }
}

export class DeletePermissionResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePermissionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePermissionResponse): DeletePermissionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePermissionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePermissionResponse;
  static deserializeBinaryFromReader(message: DeletePermissionResponse, reader: jspb.BinaryReader): DeletePermissionResponse;
}

export namespace DeletePermissionResponse {
  export type AsObject = {
  }
}

export class LoginRequest extends jspb.Message {
  getLogin(): string;
  setLogin(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LoginRequest): LoginRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginRequest;
  static deserializeBinaryFromReader(message: LoginRequest, reader: jspb.BinaryReader): LoginRequest;
}

export namespace LoginRequest {
  export type AsObject = {
    login: string,
    password: string,
  }
}

export class LoginResponse extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.Role | undefined;
  setRole(value?: v1_auth_common_dtos_pb.Role): void;

  getNeedPasswordChange(): boolean;
  setNeedPasswordChange(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LoginResponse): LoginResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginResponse;
  static deserializeBinaryFromReader(message: LoginResponse, reader: jspb.BinaryReader): LoginResponse;
}

export namespace LoginResponse {
  export type AsObject = {
    sessionToken: string,
    role?: v1_auth_common_dtos_pb.Role.AsObject,
    needPasswordChange: boolean,
  }
}

export class LogoutRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LogoutRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LogoutRequest): LogoutRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LogoutRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LogoutRequest;
  static deserializeBinaryFromReader(message: LogoutRequest, reader: jspb.BinaryReader): LogoutRequest;
}

export namespace LogoutRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateSessionRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  getServiceName(): string;
  setServiceName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionRequest): ValidateSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionRequest;
  static deserializeBinaryFromReader(message: ValidateSessionRequest, reader: jspb.BinaryReader): ValidateSessionRequest;
}

export namespace ValidateSessionRequest {
  export type AsObject = {
    sessionToken: string,
    serviceName: string,
  }
}

export class ValidateSessionResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getSessionId(): string;
  setSessionId(value: string): void;

  getStatus(): v1_auth_common_dtos_pb.StatusMap[keyof v1_auth_common_dtos_pb.StatusMap];
  setStatus(value: v1_auth_common_dtos_pb.StatusMap[keyof v1_auth_common_dtos_pb.StatusMap]): void;

  hasRole(): boolean;
  clearRole(): void;
  getRole(): v1_auth_common_dtos_pb.Role | undefined;
  setRole(value?: v1_auth_common_dtos_pb.Role): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionResponse): ValidateSessionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionResponse;
  static deserializeBinaryFromReader(message: ValidateSessionResponse, reader: jspb.BinaryReader): ValidateSessionResponse;
}

export namespace ValidateSessionResponse {
  export type AsObject = {
    userId: string,
    sessionId: string,
    status: v1_auth_common_dtos_pb.StatusMap[keyof v1_auth_common_dtos_pb.StatusMap],
    role?: v1_auth_common_dtos_pb.Role.AsObject,
  }
}

export class ValidateUserSessionRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateUserSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateUserSessionRequest): ValidateUserSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateUserSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateUserSessionRequest;
  static deserializeBinaryFromReader(message: ValidateUserSessionRequest, reader: jspb.BinaryReader): ValidateUserSessionRequest;
}

export namespace ValidateUserSessionRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateUserSessionResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateUserSessionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateUserSessionResponse): ValidateUserSessionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateUserSessionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateUserSessionResponse;
  static deserializeBinaryFromReader(message: ValidateUserSessionResponse, reader: jspb.BinaryReader): ValidateUserSessionResponse;
}

export namespace ValidateUserSessionResponse {
  export type AsObject = {
    userId: string,
  }
}

export class ValidateWsSessionRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateWsSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateWsSessionRequest): ValidateWsSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateWsSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateWsSessionRequest;
  static deserializeBinaryFromReader(message: ValidateWsSessionRequest, reader: jspb.BinaryReader): ValidateWsSessionRequest;
}

export namespace ValidateWsSessionRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateWsSessionResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateWsSessionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateWsSessionResponse): ValidateWsSessionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateWsSessionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateWsSessionResponse;
  static deserializeBinaryFromReader(message: ValidateWsSessionResponse, reader: jspb.BinaryReader): ValidateWsSessionResponse;
}

export namespace ValidateWsSessionResponse {
  export type AsObject = {
    userId: string,
  }
}

export class ResetSessionRequest extends jspb.Message {
  getSessionId(): number;
  setSessionId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResetSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ResetSessionRequest): ResetSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResetSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResetSessionRequest;
  static deserializeBinaryFromReader(message: ResetSessionRequest, reader: jspb.BinaryReader): ResetSessionRequest;
}

export namespace ResetSessionRequest {
  export type AsObject = {
    sessionId: number,
  }
}

export class ListSessionsRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SessionFilter | undefined;
  setFilter(value?: SessionFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSessionsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSessionsRequest): ListSessionsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSessionsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSessionsRequest;
  static deserializeBinaryFromReader(message: ListSessionsRequest, reader: jspb.BinaryReader): ListSessionsRequest;
}

export namespace ListSessionsRequest {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationRequest.AsObject,
    filter?: SessionFilter.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class SessionFilter extends jspb.Message {
  hasSessionId(): boolean;
  clearSessionId(): void;
  getSessionId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setSessionId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasToken(): boolean;
  clearToken(): void;
  getToken(): google_protobuf_wrappers_pb.StringValue | undefined;
  setToken(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasServiceName(): boolean;
  clearServiceName(): void;
  getServiceName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setServiceName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): v1_auth_common_common_pb.TimeRange | undefined;
  setCreatedTime(value?: v1_auth_common_common_pb.TimeRange): void;

  hasExpiredTime(): boolean;
  clearExpiredTime(): void;
  getExpiredTime(): v1_auth_common_common_pb.TimeRange | undefined;
  setExpiredTime(value?: v1_auth_common_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SessionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SessionFilter): SessionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SessionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SessionFilter;
  static deserializeBinaryFromReader(message: SessionFilter, reader: jspb.BinaryReader): SessionFilter;
}

export namespace SessionFilter {
  export type AsObject = {
    sessionId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    token?: google_protobuf_wrappers_pb.StringValue.AsObject,
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    serviceName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdTime?: v1_auth_common_common_pb.TimeRange.AsObject,
    expiredTime?: v1_auth_common_common_pb.TimeRange.AsObject,
  }
}

export class ListSessionsResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationResponse): void;

  clearSessionsList(): void;
  getSessionsList(): Array<v1_auth_common_dtos_pb.Session>;
  setSessionsList(value: Array<v1_auth_common_dtos_pb.Session>): void;
  addSessions(value?: v1_auth_common_dtos_pb.Session, index?: number): v1_auth_common_dtos_pb.Session;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSessionsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSessionsResponse): ListSessionsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSessionsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSessionsResponse;
  static deserializeBinaryFromReader(message: ListSessionsResponse, reader: jspb.BinaryReader): ListSessionsResponse;
}

export namespace ListSessionsResponse {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationResponse.AsObject,
    sessionsList: Array<v1_auth_common_dtos_pb.Session.AsObject>,
  }
}

