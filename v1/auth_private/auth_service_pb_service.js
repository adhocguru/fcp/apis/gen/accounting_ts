// package: fcp.accounting.v1.auth_private
// file: v1/auth_private/auth_service.proto

var v1_auth_private_auth_service_pb = require("../../v1/auth_private/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var UserManagementService = (function () {
  function UserManagementService() {}
  UserManagementService.serviceName = "fcp.accounting.v1.auth_private.UserManagementService";
  return UserManagementService;
}());

UserManagementService.SendCode = {
  methodName: "SendCode",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.SendCodeRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.CheckCode = {
  methodName: "CheckCode",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckCodeRequest,
  responseType: v1_auth_private_auth_service_pb.CheckCodeResponse
};

UserManagementService.CheckUserByLogin = {
  methodName: "CheckUserByLogin",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckUserByLoginRequest,
  responseType: v1_auth_private_auth_service_pb.CheckUserByLoginResponse
};

UserManagementService.GetUser = {
  methodName: "GetUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.ListUsers = {
  methodName: "ListUsers",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListUsersRequest,
  responseType: v1_auth_private_auth_service_pb.ListUsersResponse
};

UserManagementService.CreateUser = {
  methodName: "CreateUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreateUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.UpdateUser = {
  methodName: "UpdateUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.CheckPassword = {
  methodName: "CheckPassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckPasswordRequest,
  responseType: v1_auth_private_auth_service_pb.CheckPasswordResponse
};

UserManagementService.ChangePassword = {
  methodName: "ChangePassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ChangePasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.LockUser = {
  methodName: "LockUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LockUserRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.UnlockUser = {
  methodName: "UnlockUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UnlockUserRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.InitPasswordReset = {
  methodName: "InitPasswordReset",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.InitPasswordResetRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.ValidatePasswordResetToken = {
  methodName: "ValidatePasswordResetToken",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenRequest,
  responseType: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenResponse
};

UserManagementService.ResetPassword = {
  methodName: "ResetPassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ResetPasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.GetUserResetToken = {
  methodName: "GetUserResetToken",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUserResetTokenRequest,
  responseType: v1_auth_private_auth_service_pb.GetUserResetTokenResponse
};

UserManagementService.GetUsersByIds = {
  methodName: "GetUsersByIds",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUsersByIdsRequest,
  responseType: v1_auth_private_auth_service_pb.GetUsersByIdsResponse
};

UserManagementService.GetUsersByPatternForMessage = {
  methodName: "GetUsersByPatternForMessage",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageRequest,
  responseType: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageResponse
};

exports.UserManagementService = UserManagementService;

function UserManagementServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

UserManagementServiceClient.prototype.sendCode = function sendCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SendCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkCode = function checkCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkUserByLogin = function checkUserByLogin(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckUserByLogin, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listUsers = function listUsers(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListUsers, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.createUser = function createUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CreateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkPassword = function checkPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.changePassword = function changePassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ChangePassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.lockUser = function lockUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.LockUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.unlockUser = function unlockUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.UnlockUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.initPasswordReset = function initPasswordReset(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.InitPasswordReset, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.validatePasswordResetToken = function validatePasswordResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ValidatePasswordResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.resetPassword = function resetPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ResetPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUserResetToken = function getUserResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUserResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUsersByIds = function getUsersByIds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUsersByIds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUsersByPatternForMessage = function getUsersByPatternForMessage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUsersByPatternForMessage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.UserManagementServiceClient = UserManagementServiceClient;

var RoleManagementService = (function () {
  function RoleManagementService() {}
  RoleManagementService.serviceName = "fcp.accounting.v1.auth_private.RoleManagementService";
  return RoleManagementService;
}());

RoleManagementService.GetRole = {
  methodName: "GetRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetRoleRequest,
  responseType: v1_auth_private_auth_service_pb.GetRoleResponse
};

RoleManagementService.CreateRole = {
  methodName: "CreateRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreateRoleRequest,
  responseType: v1_auth_private_auth_service_pb.CreateRoleResponse
};

RoleManagementService.ListRoles = {
  methodName: "ListRoles",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListRolesRequest,
  responseType: v1_auth_private_auth_service_pb.ListRolesResponse
};

RoleManagementService.UpdateRole = {
  methodName: "UpdateRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdateRoleRequest,
  responseType: v1_auth_private_auth_service_pb.UpdateRoleResponse
};

RoleManagementService.GetPermission = {
  methodName: "GetPermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetPermissionRequest,
  responseType: v1_auth_private_auth_service_pb.GetPermissionResponse
};

RoleManagementService.CreatePermission = {
  methodName: "CreatePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreatePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.CreatePermissionResponse
};

RoleManagementService.UpdatePermission = {
  methodName: "UpdatePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdatePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.UpdatePermissionResponse
};

RoleManagementService.DeletePermission = {
  methodName: "DeletePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.DeletePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.DeletePermissionResponse
};

RoleManagementService.ListPermissions = {
  methodName: "ListPermissions",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListPermissionsRequest,
  responseType: v1_auth_private_auth_service_pb.ListPermissionsResponse
};

exports.RoleManagementService = RoleManagementService;

function RoleManagementServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

RoleManagementServiceClient.prototype.getRole = function getRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.GetRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.createRole = function createRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.CreateRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.listRoles = function listRoles(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.ListRoles, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.updateRole = function updateRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.UpdateRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.getPermission = function getPermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.GetPermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.createPermission = function createPermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.CreatePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.updatePermission = function updatePermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.UpdatePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.deletePermission = function deletePermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.DeletePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.listPermissions = function listPermissions(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.ListPermissions, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.RoleManagementServiceClient = RoleManagementServiceClient;

var AuthSessionService = (function () {
  function AuthSessionService() {}
  AuthSessionService.serviceName = "fcp.accounting.v1.auth_private.AuthSessionService";
  return AuthSessionService;
}());

AuthSessionService.Login = {
  methodName: "Login",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LoginRequest,
  responseType: v1_auth_private_auth_service_pb.LoginResponse
};

AuthSessionService.Logout = {
  methodName: "Logout",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LogoutRequest,
  responseType: google_protobuf_empty_pb.Empty
};

AuthSessionService.ValidateSession = {
  methodName: "ValidateSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateSessionResponse
};

AuthSessionService.ValidateUserSession = {
  methodName: "ValidateUserSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateUserSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateUserSessionResponse
};

AuthSessionService.ValidateWsSession = {
  methodName: "ValidateWsSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateWsSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateWsSessionResponse
};

AuthSessionService.ResetSession = {
  methodName: "ResetSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ResetSessionRequest,
  responseType: google_protobuf_empty_pb.Empty
};

AuthSessionService.ListSessions = {
  methodName: "ListSessions",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListSessionsRequest,
  responseType: v1_auth_private_auth_service_pb.ListSessionsResponse
};

exports.AuthSessionService = AuthSessionService;

function AuthSessionServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

AuthSessionServiceClient.prototype.login = function login(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.Login, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.logout = function logout(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.Logout, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateUserSession = function validateUserSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateUserSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateWsSession = function validateWsSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateWsSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.resetSession = function resetSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ResetSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.listSessions = function listSessions(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ListSessions, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.AuthSessionServiceClient = AuthSessionServiceClient;

