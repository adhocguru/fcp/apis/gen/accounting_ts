// package: fcp.accounting.v1.auth_web
// file: v1/auth_web/auth_service.proto

var v1_auth_web_auth_service_pb = require("../../v1/auth_web/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var WebAuthService = (function () {
  function WebAuthService() {}
  WebAuthService.serviceName = "fcp.accounting.v1.auth_web.WebAuthService";
  return WebAuthService;
}());

WebAuthService.GetUser = {
  methodName: "GetUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

WebAuthService.UpdateUser = {
  methodName: "UpdateUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

WebAuthService.CheckPassword = {
  methodName: "CheckPassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.CheckPasswordRequest,
  responseType: v1_auth_web_auth_service_pb.CheckPasswordResponse
};

WebAuthService.InitPasswordReset = {
  methodName: "InitPasswordReset",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
  responseType: v1_auth_web_auth_service_pb.InitPasswordResetResponse
};

WebAuthService.ValidatePasswordResetToken = {
  methodName: "ValidatePasswordResetToken",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
  responseType: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse
};

WebAuthService.ResetPassword = {
  methodName: "ResetPassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ResetPasswordRequest,
  responseType: v1_auth_web_auth_service_pb.ResetPasswordResponse
};

WebAuthService.GetUserResetToken = {
  methodName: "GetUserResetToken",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
  responseType: v1_auth_web_auth_service_pb.GetUserResetTokenResponse
};

WebAuthService.Login = {
  methodName: "Login",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.LoginRequest,
  responseType: v1_auth_web_auth_service_pb.LoginResponse
};

WebAuthService.Logout = {
  methodName: "Logout",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.LogoutRequest,
  responseType: google_protobuf_empty_pb.Empty
};

WebAuthService.ValidateSession = {
  methodName: "ValidateSession",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_web_auth_service_pb.ValidateSessionResponse
};

WebAuthService.ChangePassword = {
  methodName: "ChangePassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ChangePasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

exports.WebAuthService = WebAuthService;

function WebAuthServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

WebAuthServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.checkPassword = function checkPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.CheckPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.initPasswordReset = function initPasswordReset(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.InitPasswordReset, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.validatePasswordResetToken = function validatePasswordResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ValidatePasswordResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.resetPassword = function resetPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ResetPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getUserResetToken = function getUserResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetUserResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.login = function login(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.Login, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.logout = function logout(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.Logout, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.changePassword = function changePassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ChangePassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.WebAuthServiceClient = WebAuthServiceClient;

