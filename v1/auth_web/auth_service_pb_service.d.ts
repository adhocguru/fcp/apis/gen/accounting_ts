// package: fcp.accounting.v1.auth_web
// file: v1/auth_web/auth_service.proto

import * as v1_auth_web_auth_service_pb from "../../v1/auth_web/auth_service_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";
import {grpc} from "@improbable-eng/grpc-web";

type WebAuthServiceGetUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.GetUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type WebAuthServiceUpdateUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.UpdateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type WebAuthServiceCheckPassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.CheckPasswordRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.CheckPasswordResponse;
};

type WebAuthServiceInitPasswordReset = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.InitPasswordResetRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.InitPasswordResetResponse;
};

type WebAuthServiceValidatePasswordResetToken = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse;
};

type WebAuthServiceResetPassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ResetPasswordRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ResetPasswordResponse;
};

type WebAuthServiceGetUserResetToken = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.GetUserResetTokenRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.GetUserResetTokenResponse;
};

type WebAuthServiceLogin = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.LoginRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.LoginResponse;
};

type WebAuthServiceLogout = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.LogoutRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type WebAuthServiceValidateSession = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ValidateSessionRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ValidateSessionResponse;
};

type WebAuthServiceChangePassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ChangePasswordRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

export class WebAuthService {
  static readonly serviceName: string;
  static readonly GetUser: WebAuthServiceGetUser;
  static readonly UpdateUser: WebAuthServiceUpdateUser;
  static readonly CheckPassword: WebAuthServiceCheckPassword;
  static readonly InitPasswordReset: WebAuthServiceInitPasswordReset;
  static readonly ValidatePasswordResetToken: WebAuthServiceValidatePasswordResetToken;
  static readonly ResetPassword: WebAuthServiceResetPassword;
  static readonly GetUserResetToken: WebAuthServiceGetUserResetToken;
  static readonly Login: WebAuthServiceLogin;
  static readonly Logout: WebAuthServiceLogout;
  static readonly ValidateSession: WebAuthServiceValidateSession;
  static readonly ChangePassword: WebAuthServiceChangePassword;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class WebAuthServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getUser(
    requestMessage: v1_auth_web_auth_service_pb.GetUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_web_auth_service_pb.GetUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_web_auth_service_pb.UpdateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_web_auth_service_pb.UpdateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_web_auth_service_pb.CheckPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_web_auth_service_pb.CheckPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.InitPasswordResetResponse|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.InitPasswordResetResponse|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_web_auth_service_pb.ResetPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ResetPasswordResponse|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_web_auth_service_pb.ResetPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ResetPasswordResponse|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  login(
    requestMessage: v1_auth_web_auth_service_pb.LoginRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  login(
    requestMessage: v1_auth_web_auth_service_pb.LoginRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_web_auth_service_pb.LogoutRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_web_auth_service_pb.LogoutRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_web_auth_service_pb.ValidateSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_web_auth_service_pb.ValidateSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_web_auth_service_pb.ChangePasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_web_auth_service_pb.ChangePasswordRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
}

