// package: fcp.accounting.v1
// file: v1/auth_common/dtos.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class User extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getChangePassword(): boolean;
  setChangePassword(value: boolean): void;

  clearContactsList(): void;
  getContactsList(): Array<string>;
  setContactsList(value: Array<string>): void;
  addContacts(value: string, index?: number): string;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    id: string,
    status: number,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    changePassword: boolean,
    contactsList: Array<string>,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class UserBriefInfo extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getChangePassword(): boolean;
  setChangePassword(value: boolean): void;

  clearContactsList(): void;
  getContactsList(): Array<string>;
  setContactsList(value: Array<string>): void;
  addContacts(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserBriefInfo.AsObject;
  static toObject(includeInstance: boolean, msg: UserBriefInfo): UserBriefInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserBriefInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserBriefInfo;
  static deserializeBinaryFromReader(message: UserBriefInfo, reader: jspb.BinaryReader): UserBriefInfo;
}

export namespace UserBriefInfo {
  export type AsObject = {
    id: string,
    status: number,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    changePassword: boolean,
    contactsList: Array<string>,
  }
}

export class Role extends jspb.Message {
  getRole(): string;
  setRole(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearPermissionsList(): void;
  getPermissionsList(): Array<Permission>;
  setPermissionsList(value: Array<Permission>): void;
  addPermissions(value?: Permission, index?: number): Permission;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Role.AsObject;
  static toObject(includeInstance: boolean, msg: Role): Role.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Role, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Role;
  static deserializeBinaryFromReader(message: Role, reader: jspb.BinaryReader): Role;
}

export namespace Role {
  export type AsObject = {
    role: string,
    description: string,
    permissionsList: Array<Permission.AsObject>,
  }
}

export class Permission extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Permission.AsObject;
  static toObject(includeInstance: boolean, msg: Permission): Permission.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Permission, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Permission;
  static deserializeBinaryFromReader(message: Permission, reader: jspb.BinaryReader): Permission;
}

export namespace Permission {
  export type AsObject = {
    permission: string,
    description: string,
  }
}

export class RoleFull extends jspb.Message {
  getRole(): string;
  setRole(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearPermissionsList(): void;
  getPermissionsList(): Array<PermissionFull>;
  setPermissionsList(value: Array<PermissionFull>): void;
  addPermissions(value?: PermissionFull, index?: number): PermissionFull;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleFull.AsObject;
  static toObject(includeInstance: boolean, msg: RoleFull): RoleFull.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoleFull, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleFull;
  static deserializeBinaryFromReader(message: RoleFull, reader: jspb.BinaryReader): RoleFull;
}

export namespace RoleFull {
  export type AsObject = {
    role: string,
    description: string,
    permissionsList: Array<PermissionFull.AsObject>,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class PermissionFull extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PermissionFull.AsObject;
  static toObject(includeInstance: boolean, msg: PermissionFull): PermissionFull.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PermissionFull, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PermissionFull;
  static deserializeBinaryFromReader(message: PermissionFull, reader: jspb.BinaryReader): PermissionFull;
}

export namespace PermissionFull {
  export type AsObject = {
    permission: string,
    description: string,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class Session extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getToken(): string;
  setToken(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getServiceName(): string;
  setServiceName(value: string): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasExpiredTime(): boolean;
  clearExpiredTime(): void;
  getExpiredTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setExpiredTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Session.AsObject;
  static toObject(includeInstance: boolean, msg: Session): Session.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Session, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Session;
  static deserializeBinaryFromReader(message: Session, reader: jspb.BinaryReader): Session;
}

export namespace Session {
  export type AsObject = {
    id: number,
    token: string,
    userId: string,
    serviceName: string,
    createdTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    expiredTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export interface RoleTypeMap {
  ADMIN: 0;
  MANAGER: 1;
  FARMER: 2;
  AGENT: 3;
  SUPPLIER: 4;
  MERCHANDISER: 5;
  DESTRIBUTER: 6;
  PROVIDER: 7;
  TRADER: 8;
  PROCESSOR: 9;
  RETAILER: 10;
  BANK: 11;
  INSURER: 12;
  SUPPORTER: 13;
}

export const RoleType: RoleTypeMap;

export interface StatusMap {
  NEW: 0;
  AUTHORIZED: 1;
  BLOCKED: 2;
}

export const Status: StatusMap;

