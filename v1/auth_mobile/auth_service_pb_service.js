// package: fcp.accounting.v1.auth_mobile
// file: v1/auth_mobile/auth_service.proto

var v1_auth_mobile_auth_service_pb = require("../../v1/auth_mobile/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MobileAuthService = (function () {
  function MobileAuthService() {}
  MobileAuthService.serviceName = "fcp.accounting.v1.auth_mobile.MobileAuthService";
  return MobileAuthService;
}());

MobileAuthService.SendCode = {
  methodName: "SendCode",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.SendCodeRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.CheckCode = {
  methodName: "CheckCode",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
  responseType: v1_auth_mobile_auth_service_pb.CheckCodeResponse
};

MobileAuthService.GetUser = {
  methodName: "GetUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

MobileAuthService.RegisterUser = {
  methodName: "RegisterUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
  responseType: v1_auth_mobile_auth_service_pb.RegisterUserResponse
};

MobileAuthService.UpdateUser = {
  methodName: "UpdateUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

MobileAuthService.ValidateSession = {
  methodName: "ValidateSession",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_mobile_auth_service_pb.ValidateSessionResponse
};

exports.MobileAuthService = MobileAuthService;

function MobileAuthServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MobileAuthServiceClient.prototype.sendCode = function sendCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SendCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.checkCode = function checkCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.CheckCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.registerUser = function registerUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.RegisterUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MobileAuthServiceClient = MobileAuthServiceClient;

