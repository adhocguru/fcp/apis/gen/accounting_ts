// package: fcp.accounting.v1.auth_mobile
// file: v1/auth_mobile/auth_service.proto

import * as v1_auth_mobile_auth_service_pb from "../../v1/auth_mobile/auth_service_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MobileAuthServiceSendCode = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.SendCodeRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceCheckCode = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.CheckCodeRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.CheckCodeResponse;
};

type MobileAuthServiceGetUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.GetUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type MobileAuthServiceRegisterUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.RegisterUserRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.RegisterUserResponse;
};

type MobileAuthServiceUpdateUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.UpdateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type MobileAuthServiceValidateSession = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.ValidateSessionRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.ValidateSessionResponse;
};

export class MobileAuthService {
  static readonly serviceName: string;
  static readonly SendCode: MobileAuthServiceSendCode;
  static readonly CheckCode: MobileAuthServiceCheckCode;
  static readonly GetUser: MobileAuthServiceGetUser;
  static readonly RegisterUser: MobileAuthServiceRegisterUser;
  static readonly UpdateUser: MobileAuthServiceUpdateUser;
  static readonly ValidateSession: MobileAuthServiceValidateSession;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MobileAuthServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  sendCode(
    requestMessage: v1_auth_mobile_auth_service_pb.SendCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  sendCode(
    requestMessage: v1_auth_mobile_auth_service_pb.SendCodeRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_mobile_auth_service_pb.GetUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_mobile_auth_service_pb.GetUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  registerUser(
    requestMessage: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.RegisterUserResponse|null) => void
  ): UnaryResponse;
  registerUser(
    requestMessage: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.RegisterUserResponse|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
}

